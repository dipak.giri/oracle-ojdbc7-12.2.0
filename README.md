# Install local jar to local maven repo
```
mvn install:install-file \
   -Dfile=ojdbc7-12.2.0.jar \
   -DgroupId=com.oracle \
   -DartifactId=ojdbc7 \
   -Dversion=12.2.0 \
   -Dpackaging=jar \
   -DgeneratePom=true
```
